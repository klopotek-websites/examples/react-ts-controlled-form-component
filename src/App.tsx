import React from 'react';
import './App.css';

import { useState } from 'react';

function App() {

  const [score, setScore] = useState("10")
  const [comment, setComment] = useState("");

  function handleSubmit(e: React.ChangeEvent<HTMLFormElement>): void {
    e.preventDefault();
    if (Number(score) < 5 && comment.length < 10) {
      alert("Please provide a comment why the expierience was so low.");
      return
    }
    console.log("Form submitted");

    setScore("10");
    setComment("");
    alert("Thank You for the feedback!");
    return;
  }

  return (
    <div className="App">
      <form onSubmit={handleSubmit} className="form">
        <h2> Feedback Form</h2>
        <div className="field">
          <label>Score {score} {'\u2B50'}</label>
          <input
            type="range"
            id="score"
            value={score}
            min="0"
            step="0.5"
            max="10"
            onChange={(e) => setScore(e.target.value)}
          />
        </div>
        <div className="field">
          <label>Comment</label>
          <textarea rows={4} cols={50} value={comment} onChange={(e) => setComment(e.target.value)} />
        </div>
        <button type="submit" className="btn-primary">Submit</button>
      </form>
    </div>
  );
}

export default App;
