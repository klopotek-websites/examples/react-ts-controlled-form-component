
# React.js controlled form: React.js + TypeScript

Example React app showcasing a controlled form component.
Created using:

- React.js
- TypeScript


This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).



## Layout

### Standard size
![WebsiteLayout-1](./react-ts-controlled-form-component-layout.png)
